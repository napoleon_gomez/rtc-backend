# Ripley Technical Challenge - Backend

Proyecto del lado backend para el Desafío Técnico de Ripley, utiliza Node.js version 14.15.1.

## Development server

Para echar a correr el proyecto, utilizar el comando `nodemon index` en la raíz del proyecto.
En tu navegador, dirígete a `http://localhost:8082/api`, y si recibes un mensaje de éxito, el backend está funcionando.
