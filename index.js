let express = require('express'); // Import express
let bodyParser = require('body-parser'); // Import body-parser
let mongoose = require('mongoose'); // Import mongoose
let cors = require('cors');

let routes = require("./src/routes/userRoutes"); // Import routes

// Initialize the app
let app = express();
app.use(bodyParser.urlencoded({
   extended: true
}));
app.use(bodyParser.json());
app.use(cors());

// mongoose.connect('mongodb://localhost/rtc-mongo', { useNewUrlParser: true}); // Conexion local
mongoose.connect('mongodb+srv://root:SoulEdge13@cluster0.uiqxu.mongodb.net/rtc-mongo?retryWrites=true&w=majority', { useNewUrlParser: true}); // Conexion a MongoDB Atlas
var db = mongoose.connection;

if(!db) { console.log("Error connecting db");
} else { console.log("Db connected successfully"); }

var port = process.env.PORT || 8080; // Setup server port

app.get('/', (req, res) => res.status(403).send('Forbidden page')); // Send message for default URL

app.use('/api', routes); // Use api routes in the APP

app.listen(port, function () { console.log("Running RTC on port " + port); }); // Launch app to listen to specified port
