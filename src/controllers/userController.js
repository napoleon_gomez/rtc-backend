User = require('../models/userModel'); // Importa el modelo del usuario
const bcrypt = require('bcrypt'); // Importa el encriptador
const salt = 10; // Constante de nivel de seguridad de encriptado

// Método para la creación de nuevos usuarios, validando que ningún campo esté vacio, y que el RUT no se repita
exports.register = function (req, res) {
  var user = new User();
  // Encapsulado en esta función para generar una password encriptada
  bcrypt.hash(req.body.password, salt).then(function(hash) {
    user.name = req.body.name ? req.body.name : user.name;
    user.rut = req.body.rut;
    user.email = req.body.email;
    user.password = hash;
    if(user.name != '' && user.rut != '' && user.email != '' && user.password != '') {
      User.findOne({$or: [{'rut': user.rut}, {'email': user.email}]}, function (err, docs) {
        if(docs == null) {
          user.save(function (err) {
            if (err) { res.json(err); }
            res.json({
              message: '¡Registro de usuario exitoso!',
              status: true,
              data: {
                id: user._id,
                name: user.name,
                balance: user.balance
              }
            });
          });
        } else {
          res.json({
            message: 'Lo sentimos. Este RUT o email ya se encuentra registrado en nuestro sistema. Por favor, intenta con uno distinto.',
            status: false,
            data: {}
          });
        }
      });
    } else {
      res.json({
        message: '¡Todos los campos son requeridos!',
        status: false,
        data: {}
      });
    }
  });
};

// Método para entrar al sistema, validando que ningún campo esté vacio
exports.login = function (req, res) {
  if(req.body.email != '' && req.body.password != '') {
    User.findOne({'email': req.body.email}, function (err, docs) {
      if(docs != null) {
        bcrypt.compare(req.body.password, docs.password, function (err, result) {
          if (result == true) {
            res.json({
              message: 'Logueado en el sistema',
              status: true,
              data: {
                id: docs._id,
                name: docs.name,
                balance: docs.balance
              }
            });
          } else {
            res.json({
              message: 'Contraseña incorrecta',
              status: false,
              data: {}
            });
          }
        });
      } else {
        res.json({
          message: 'Lo sentimos. Este email no se encuentra registrado.',
          status: false,
          data: {}
        });
      }
    });
  } else {
    res.json({
      message: '¡Todos los campos son requeridos!',
      status: false,
      data: {}
    });
  }
};

// Método para obtener balance
exports.balance = function (req, res) {
  if(req.body.id != '') {
    User.findOne({'_id':req.body.id}, function (err, docs) {
      if(docs != null) {
        let balance = docs.balance != null && docs.balance != undefined ? docs.balance : 0;
        res.json({
          message: 'Tu saldo es: ' + docs.balance,
          status: true,
          data: {
            balance: balance
          }
        });
      } else {
        res.json({
          message: 'Ha ocurrido un error al intentar obtener tu saldo.',
          status: false,
          data: {}
        });
      }
    });
  } else {
    res.json({
      message: '¡Todos los campos son requeridos!',
      status: false,
      data: {}
    });
  }
};

// Método para depositar monto
exports.deposit = function (req, res) {
  if(req.body.id != '' && req.body.amount != '') {
    let deposit = !isNaN(parseFloat(req.body.amount)) ? parseFloat(req.body.amount) : 0;
    if(deposit > 0) {
      User.findOneAndUpdate({'_id':req.body.id},{$inc:{'balance':deposit},$push:{'transactions':{'type': 'Depósito','amount': deposit}}},{new:true}, function (err, docs) {
        if(docs != null) {
          res.json({
            message: '¡Tu monto ha sido depositado!',
            status: true,
            data: {
              balance: docs.balance
            }
          });
        } else {
          res.json({
            message: 'Ha ocurrido un error al intentar depositar a tu cuenta.',
            status: false,
            data: {}
          });
        }
      });
    } else {
      res.json({
        message: 'Monto inválido',
        status: false,
        data: {}
      });
    }
  } else {
    res.json({
      message: '¡Todos los campos son requeridos!',
      status: false,
      data: {}
    });
  }
};

// Método para retirar monto
exports.withdraw = function (req, res) {
  if(req.body.id != '' && req.body.amount != '') {
    let withdraw = !isNaN(parseFloat(req.body.amount)) ? parseFloat(req.body.amount) : 0;
    if(withdraw > 0) {
      User.findOneAndUpdate({'_id':req.body.id, 'balance':{$gte:req.body.amount}}, {$inc:{'balance':-withdraw},$push:{'transactions':{'type': 'Retiro','amount': withdraw}}}, {new:true}, function (err, docs) {
        if(docs != null) {
          res.json({
            message: '¡Retiro exitoso!',
            status: true,
            data: {
              balance: docs.balance
            }
          });
        } else {
          res.json({
            message: 'El monto a retirar no puede ser mayor al saldo de la cuenta.',
            status: false,
            data: {}
          });
        }
      });
    } else {
      res.json({
        message: 'Monto inválido',
        status: false,
        data: {}
      });
    }
  } else {
    res.json({
      message: '¡Todos los campos son requeridos!',
      status: false,
      data: {}
    });
  }
};

// Método para transferir monto
exports.transfer = function (req, res) {
  if(req.body.id != '' && req.body.amount != '' && req.body.rut != '') {
    let transfer = !isNaN(parseFloat(req.body.amount)) ? parseFloat(req.body.amount) : 0;
    if(transfer > 0) {
      User.findOne({'rut':req.body.rut}, function (err, docs) {
        if(docs != null) {
          User.findOneAndUpdate({'_id':req.body.id, 'balance':{$gte:req.body.amount}}, {$inc:{'balance':-transfer},$push:{'transactions':{'type': 'Transferencia enviada','amount': transfer}}}, {new:true}, function (err, originAccount) {
            if(originAccount != null) {
              User.findOneAndUpdate({'_id':docs.id}, {$inc:{'balance':transfer},$push:{'transactions':{'type': 'Transferencia recibida','amount': transfer}}}, {new:true}, function (err, destinyAccount) {
                if(destinyAccount != null) {
                  res.json({
                    message: '¡Transferencia exitosa!',
                    status: true,
                    data: {
                      balance: originAccount.balance
                    }
                  });
                } else {
                  res.json({
                    message: 'Lo sentimos, no pudimos encontrar la cuenta de destino.',
                    status: false,
                    data: {}
                  });
                }
              });
            } else {
              res.json({
                message: 'Tu balance es insuficiente para hacer esta operación.',
                status: false,
                data: {}
              });
            }
          });
        } else {
          res.json({
            message: 'La cuenta de destino no existe.',
            status: false,
            data: {}
          });
        }
      });
    } else {
      res.json({
        message: 'Monto inválido',
        status: false,
        data: {}
      });
    }
  } else {
    res.json({
      message: '¡Todos los campos son requeridos!',
      status: false,
      data: {}
    });
  }
};

// Exporta las transacciones de un usuario
exports.transactions = function (req, res) {
  User.findById(req.body.id, function (err, docs) {
    if (err) { res.send(err); }
    res.json({
      message: 'Consulta exitosa',
      status: true,
      data: {
        balance: docs.balance,
        transactions: docs.transactions
      }
    });
  });
};
