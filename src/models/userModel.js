var mongoose = require('mongoose');

var transactionsSchema = mongoose.Schema({
  amount: {
    type: Number
  },
  type: {
    type: String
  },
  date: {
    type: Date,
    default: Date.now
  }
});

var contactSchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  rut: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  balance: {
    type: Number
  },
  transactions: [transactionsSchema],
  create_date: {
    type: Date,
    default: Date.now
  }
});

var User = module.exports = mongoose.model('user', contactSchema); // Exportar modelo User
module.exports.get = function (callback, limit) {
  User.find(callback).limit(limit);
}
