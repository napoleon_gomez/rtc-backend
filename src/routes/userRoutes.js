let router = require('express').Router(); // Initialize express router

// Set default API response
router.get('/', function (req, res) {
  res.json({
    status: '¡Éxito!',
    message: 'Las APIs están funcionando'
  });
});

var userController = require('../controllers/userController');

router.route('/register')
  // .get(userController.index)
  .post(userController.register);

router.route('/login')
  .post(userController.login);

router.route('/balance')
  .post(userController.balance);

router.route('/deposit')
  .post(userController.deposit);

router.route('/withdraw')
  .post(userController.withdraw);

router.route('/transfer')
  .post(userController.transfer);

router.route('/transactions')
  .post(userController.transactions);

module.exports = router; // Exporta las rutas de la API
